#include "EntityAtlas.h"

std::vector<EntityProperties> EntityAtlas::entityProperties = {};

void EntityAtlas::Load() {
	//TODO: Loading
	// Arrow
	entityProperties.push_back(EntityProperties{ (int)EntityType::Arrow, sf::Vector2f(16.0f, 6.0f), true, sf::IntRect(0, 0, 16, 16), 60 });
	// Slime
	entityProperties.push_back(EntityProperties{ (int)EntityType::Slime, sf::Vector2f(24.0f, 24.0f), false, sf::IntRect(32, 0, 24, 24) });
	// Coin
	entityProperties.push_back(EntityProperties{ (int)EntityType::Player, sf::Vector2f(8.0f, 8.0f), false, sf::IntRect(64, 0, 8, 8), 6000 });
}


void EntityAtlas::SetValues(Entity& entity) {
	EntityProperties properties = entityProperties[(int)entity.type];
	entity.size = properties.size;
	entity.directionDependent = properties.directionDependent;
	entity.timeToLive = properties.timeToLive;
}

sf::IntRect EntityAtlas::GetTextureRect(Entity& entity) {
	return entityProperties[(int)entity.type].textureRect;
}