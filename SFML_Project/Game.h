#pragma once

#include <SFML/Graphics.hpp>
#include <chrono>
#include <iostream>
#include <vector>

#include "FastNoise.h"
#include "Map.h"
#include "Player.h"
#include "Lighting.h"
#include "KeyState.h"
#include "Util.h"
#include "ParticleManager.h"
#include "EntityAtlas.h"
#include "EntityManager.h"

class Game {

	friend class Entity;
	friend class Player;
	friend class Lighting;
	friend class ParticleManager;
	friend class EntityManager;

private:

	sf::RenderWindow* window;
	sf::RenderTexture renderTexture;
	sf::Clock* clock;
	Map* map;
	Player* player;
	Lighting* lighting;
	KeyState* keyboardState;
	sf::Vector2i mousePosition;
	bool charging = false;
	sf::Vector2i chargeBeginPosition;
	float arrowSpeedMin = 1.0f;
	float arrowSpeedMax = 4.0f;
	sf::Time lastTime;
	sf::Font font;
	bool playing;
	bool drawAABB = false;
	bool raining = false;
	bool underRoof = false;
	float timeOfDay = 8.0f;
	float secondsPassed = 0.0f;
	float frameTime;
	float lag = 0.0f;
	const float TARGET_FRAME_TIME = 0.0167f;
	const float TILE_WIDTH = 32.0f;
	const float TILE_HEIGHT = 32.0f;
	int WINDOW_WIDTH = 960;
	int WINDOW_HEIGHT = 640;
	int WINDOW_WIDTH_TILES = (int)std::floorf(WINDOW_WIDTH / TILE_WIDTH);
	int WINDOW_HEIGHT_TILES = (int)std::floorf(WINDOW_HEIGHT / TILE_HEIGHT);
	sf::Vector2i tileTopLeft;
	sf::Vector2f renderTopLeft;
	sf::Vector2f renderOffset;
	sf::Texture tilesFloorTexture;
	sf::Texture tilesWallTexture;
	sf::Texture playerTexture;
	sf::Shader ditherShader;
	EntityManager entityManager;
	ParticleManager particleManager;

	void GetKeyboardState();
	void HandleMousePress(const sf::Event::MouseButtonEvent&);
	void HandleMouseRelease(const sf::Event::MouseButtonEvent&);
	void HandleMouseMove(const sf::Event&);
	void ProcessInput();
	void DrawFPS();
	void DrawMouseCoords();

	void DebugMessage(const std::string&) const;


public:
	Game();
	void Initialize();
	void MainLoop();
	~Game();
};

