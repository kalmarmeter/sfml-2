#pragma once

#include <SFML/System/Vector2.hpp>

#include <cmath>
#include <ctime>
#include <random>

class Util
{
private:
	static std::default_random_engine randomEngine;
public:
	static float Magnitude(const sf::Vector2f&);
	static sf::Vector2f Normalize(const sf::Vector2f&);
	static float VectorToDirection(const sf::Vector2f&);
	static sf::Vector2f DirectionToVector(float);
	static int ManhattanDistance(int x1, int y1, int x2, int y2);
	static int ManhattanDistance(const sf::Vector2i& vec1, const sf::Vector2i& vec2);
	static int RandomInt(int min, int max);
	static float RandomFloat(float min, float max);
	static sf::Vector2f RandomVec(const sf::Vector2f& center, float xMax, float yMax);
	static int Lerp(int a, int b, float t);
	static float Lerp(float a, float b, float t);
	template <typename T>
	static void Clamp(T min, T max, T& value);
	static float Map(float minSrc, float maxSrc, float minDest, float maxDest, float value);
	static constexpr float PI = 3.14159f;
	static constexpr float RAD = 57.2957795f;
	static constexpr float PI_2 = PI / 2.0f;
	static constexpr float PI_3 = PI / 3.0f;
	static constexpr float PI_4 = PI / 4.0f;
	static constexpr float TWO_PI = PI * 2.0f;
};

template <typename T>
void Util::Clamp(T min, T max, T& value) {
	value = std::max(min, std::min(max, value));
}

