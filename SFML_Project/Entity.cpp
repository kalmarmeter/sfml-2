#include "Entity.h"

Entity::Entity(EntityType type, sf::Vector2f pos, float dir, sf::Vector2f size, Game& game, bool directionDependentCollision) : game(game) {
	directionDependent = directionDependentCollision;
	this->type = type;
	if (directionDependent) {
		sf::Transform transform;
		transform.rotate(-dir * Util::RAD, pos);
		sf::Vector2f points[4] = {	sf::Vector2f(pos.x - size.x / 2.0f, pos.y - size.y / 2.0f), 
									sf::Vector2f(pos.x + size.x / 2.0f, pos.y - size.y / 2.0f),
									sf::Vector2f(pos.x + size.x / 2.0f, pos.y + size.y / 2.0f),
									sf::Vector2f(pos.x - size.x / 2.0f, pos.y + size.y / 2.0f) };
		float minX = 1'000'000.0f;
		float minY = 1'000'000.0f;
		float maxX = 0.0f;
		float maxY = 0.0f;
		for (int i = 0; i < 4; ++i) {
			points[i] = transform.transformPoint(points[i]);
			if (points[i].x < minX) minX = points[i].x;
			if (points[i].y < minY) minY = points[i].y;
			if (points[i].x > maxX) maxX = points[i].x;
			if (points[i].y > maxY) maxY = points[i].y;
		}
		AABB = sf::FloatRect(minX, minY, maxX - minX, maxY - minY);
		AABBOffset = sf::Vector2f(AABB.left, AABB.top) - pos;
	}
	else {
		AABB = sf::FloatRect(pos.x - size.x / 2.0f, pos.y - size.y / 2.0f, size.x, size.y);
	}
	position = pos;
	direction = dir;
	this->size = size;
	velocity = sf::Vector2f(0.0f, 0.0f);
}

void Entity::UpdateAABB() {
	if (directionDependent) {
		AABB = sf::FloatRect(position.x + AABBOffset.x, position.y + AABBOffset.y, AABB.width, AABB.height);
	}
	else {
		AABB = sf::FloatRect(position.x - size.x / 2.0f, position.y - size.y / 2.0f, size.x, size.y);
	}
}

void Entity::Update() {

	if (!inactive && timeToLive != -1) {
		--timeToLive;
		if (timeToLive <= 0) {
			inactive = true;
		}
	}

	// x value of -1 signals no collision
	tileCollision = sf::Vector2i(-1, 0);
	entityCollision = nullptr;
	playerCollision = false;

	CheckTileCollisions();
	CheckEntityCollisions();
	CheckPlayerCollision();

}

void Entity::CheckTileCollisions() {
	if (!inactive) {

		//Check for out of bounds
		if (position.x < 0.0f || position.y < 0.0f || position.x >(Level::MAX_WIDTH - 1) * game.TILE_WIDTH || position.y >(Level::MAX_HEIGHT - 1) * game.TILE_HEIGHT) {
			inactive = true;

			return;
		}

		//Check for AABB collisions

		//Check X
		position.x += velocity.x;
		UpdateAABB();

		int left = (int)((AABB.left) / game.TILE_WIDTH);
		int top = (int)((AABB.top) / game.TILE_HEIGHT);
		int right = (int)((AABB.left + AABB.width) / game.TILE_WIDTH);
		int bottom = (int)((AABB.top + AABB.height) / game.TILE_HEIGHT);

		for (int xx = left; xx <= right; ++xx) {
			for (int yy = top; yy <= bottom; ++yy) {

				if (game.map->IsValid(xx, yy, 1) && game.map->At(xx, yy, 1).solid) {
					sf::FloatRect rect = sf::FloatRect(xx * game.TILE_WIDTH, yy * game.TILE_HEIGHT, game.TILE_WIDTH, game.TILE_HEIGHT);
					sf::FloatRect intersection;
					if (AABB.intersects(rect, intersection)) {
						if (AABB.left < rect.left) {
							position.x -= intersection.width;
						}
						else {
							position.x += intersection.width;
						}
						tileCollision = sf::Vector2i(xx, yy);
					}
				}

			}
		}

		//Check Y
		position.y += velocity.y;
		UpdateAABB();

		left = (int)((AABB.left) / game.TILE_WIDTH);
		top = (int)((AABB.top) / game.TILE_HEIGHT);
		right = (int)((AABB.left + AABB.width) / game.TILE_WIDTH);
		bottom = (int)((AABB.top + AABB.height) / game.TILE_HEIGHT);

		for (int xx = left; xx <= right; ++xx) {
			for (int yy = top; yy <= bottom; ++yy) {

				if (game.map->IsValid(xx, yy, 1) && game.map->At(xx, yy, 1).solid) {
					sf::FloatRect rect = sf::FloatRect(xx * game.TILE_WIDTH, yy * game.TILE_HEIGHT, game.TILE_WIDTH, game.TILE_HEIGHT);
					sf::FloatRect intersection;
					if (AABB.intersects(rect, intersection)) {
						if (AABB.top < rect.top) {
							position.y -= intersection.height;
						}
						else {
							position.y += intersection.height;
						}
						tileCollision = sf::Vector2i(xx, yy);
					}
				}

			}
		}

		UpdateAABB();

	}
}

void Entity::CheckEntityCollisions() {

	if (!inactive) {
		for (Entity& other : game.entityManager.entities) {
			if (&other != this && !other.inactive && AABB.intersects(other.AABB)) {
				entityCollision = &other;
			}
		}
	}

}

void Entity::CheckPlayerCollision() {
	if (!inactive && AABB.intersects(game.player->AABB)) {
		playerCollision = true;
	}
}

Entity& Entity::operator=(const Entity& other) {
	directionDependent = other.directionDependent;
	direction = other.direction;
	position = other.position;
	size = other.size;
	velocity = other.velocity;
	AABB = other.AABB;
	AABBOffset = other.AABBOffset;
	inactive = other.inactive;
	type = other.type;

	return *this;
}

void Entity::AddVelocity(const sf::Vector2f& velocityToAdd) {
	velocity += velocityToAdd;
}

void Entity::SetVelocity(const sf::Vector2f& newVelocity) {
	velocity = newVelocity;
}

Entity::~Entity() {
}
