#include "EntityManager.h"



EntityManager::EntityManager(Game& game) : game(game) {
	entities.reserve(MAX_ENTITIES);
	texture.loadFromFile("../SFML_Project/Resources/entities.png");
}

Entity* EntityManager::NewEntity(EntityType type, sf::Vector2f pos, float dir) {
	return NewEntity(Entity(type, pos, dir, sf::Vector2f(16.0f, 16.0f), game, false));
}

Entity* EntityManager::NewEntity(const Entity& entity) {
	for (int i = 0; i < MAX_ENTITIES; ++i) {
		if (i < entities.size() && entities[i].inactive) {
			entities[i] = Entity(entity);
			EntityAtlas::SetValues(entities[i]);
			return &entities[i];
		}
		if (i >= entities.size() && i < MAX_ENTITIES) {
			entities.push_back(Entity(entity));
			EntityAtlas::SetValues(entities[i]);
			return &entities.back();
		}
	}
	return nullptr;
}

void EntityManager::Clear() {
	entities.clear();
}

void EntityManager::Update() {
	for (Entity& entity : entities) {
		if (!entity.inactive) {
			entity.Update();
			if (entity.tileCollision.x != -1) {
				if (entity.type == EntityType::Arrow) {
					entity.SetVelocity(sf::Vector2f());
					game.particleManager.NewParticles(5, sf::Vector2f(entity.position), -entity.direction + Util::PI, Util::PI_2, 5.0f, sf::Vector2f(5.0f, 5.0f), game.map->At(entity.tileCollision.x, entity.tileCollision.y, 1).color, 2.0f);
				}
			}
			if (entity.entityCollision) {
				if (entity.type == EntityType::Arrow && entity.entityCollision->type == EntityType::Slime) {
					entity.entityCollision->inactive = true;
					game.particleManager.NewParticles(10, entity.entityCollision->position, 0.0f, Util::PI * 2.0f, 3.0f, sf::Vector2f(5.0f, 5.0f), sf::Color::Green, 1.5f);
					for (int i = 0; i < 5; ++i) {
						game.entityManager.NewEntity(EntityType::Coin, Util::RandomVec(entity.entityCollision->position, 16.0f, 16.0f), 0.0f);
					}
				}
			}
			if (entity.playerCollision) {
				if (entity.type == EntityType::Coin) {
					entity.inactive = true;
					++game.player->money;
				}
			}
		}
	}
}

void EntityManager::draw(sf::RenderTarget& target, sf::RenderStates states) const {

	sf::Sprite spr;
	spr.setTexture(texture);

	for (Entity entity : entities) {
		if (!entity.inactive) {
			entity.position -= game.renderTopLeft;
			spr.setOrigin(entity.size.x / 2.0f, entity.size.y / 2.0f);
			spr.setTextureRect(EntityAtlas::GetTextureRect(entity));
			spr.setRotation(-entity.direction * Util::RAD);
			spr.setPosition(entity.position);

			sf::Text debugText(std::to_string(entity.timeToLive), game.font);
			debugText.setPosition(entity.position + sf::Vector2f(0.0f, -24.0f));

			target.draw(debugText);

			target.draw(spr);
		}
	}

}

EntityManager::~EntityManager() {
}
