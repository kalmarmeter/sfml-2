#pragma once

#include <SFML/Graphics.hpp>

#include "TileType.h"

struct Tile {

	TileType type;
	bool solid = false;
	sf::Color color;
	bool lightSource = false;
	int lightLevel = 0;
	bool ambientLight = false;
	sf::Uint8 neighbours = 0;

	Tile(TileType type = TileType::Air);

};

