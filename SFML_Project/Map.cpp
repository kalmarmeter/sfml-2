#include "Map.h"

Map::Map() {
	levels = new Level[Level::MAX_LEVELS];
	for (int i = 0; i < Level::MAX_LEVELS; ++i) {
		levels[i].Generate();
	}
	currentLevel = 0;
}

const Tile& Map::At(int x, int y, int z) const {
	return levels[currentLevel].At(x, y, z);
}

bool& Map::Roof(int x, int y) {
	return levels[currentLevel].Roof(x, y);
}

void Map::Set(int x, int y, int z, Tile newTile) {
	levels[currentLevel].Set(x, y, z, newTile);
}

bool Map::IsValid(int x, int y, int z) const {
	return levels[currentLevel].IsValid(x, y, z);
}

int Map::GetCurrentLevel() const {
	return currentLevel;
}


Map::~Map() {
	if (levels) delete[] levels;
}
