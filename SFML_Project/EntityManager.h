#pragma once

#include <vector>

#include <SFML/Graphics.hpp>

#include "EntityType.h"

class Game;
class Entity;

class EntityManager : public sf::Drawable {
	friend class Entity;

private:
	std::vector<Entity> entities;
	static const int MAX_ENTITIES = 1'000;
	Game& game;
	sf::Texture texture;
public:
	EntityManager(Game& game);
	Entity* NewEntity(EntityType type, sf::Vector2f pos, float dir);
	Entity* NewEntity(const Entity& entity);
	void Clear();
	void Update();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	~EntityManager();
};

#include "EntityAtlas.h"
