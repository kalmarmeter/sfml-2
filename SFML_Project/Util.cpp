#include "Util.h"

std::default_random_engine Util::randomEngine = std::default_random_engine(time(0));

float Util::Magnitude(const sf::Vector2f& vec) {
	return std::sqrtf(vec.x * vec.x + vec.y * vec.y);
}

sf::Vector2f Util::Normalize(const sf::Vector2f& vec) {
	return vec / Magnitude(vec);
}

float Util::VectorToDirection(const sf::Vector2f& vec) {
	return std::atan2f(vec.y, vec.x);
}

sf::Vector2f Util::DirectionToVector(float dir) {
	return sf::Vector2f(std::cos(dir), std::sin(dir));
}

int Util::ManhattanDistance(int x1, int y1, int x2, int y2) {
	if (x2 < x1) std::swap(x1, x2);
	if (y2 < y1) std::swap(y1, y2);
	return (x2 - x1) + (y2 - y1);
}

int Util::ManhattanDistance(const sf::Vector2i& vec1, const sf::Vector2i& vec2) {
	return ManhattanDistance(vec1.x, vec1.y, vec2.x, vec2.y);
}

int Util::RandomInt(int min, int max) {
	if (min == max) return min;
	return randomEngine() % (max - min + 1) + min;
}

float Util::RandomFloat(float min, float max) {
	std::uniform_real_distribution<float> dist(min, max);
	return dist(randomEngine);
}

sf::Vector2f Util::RandomVec(const sf::Vector2f& center, float xMax, float yMax) {
	return center + sf::Vector2f(RandomFloat(-xMax, xMax), RandomFloat(-yMax, yMax));
}

int Util::Lerp(int a, int b, float t) {
	if (a == b) return a;
	return std::round(a + t * (b - a));
}

float Util::Lerp(float a, float b, float t) {
	return a + t * (b - a);
}

float Util::Map(float minSrc, float maxSrc, float minDest, float maxDest, float value) {
	float t = (value - minSrc) / (maxSrc - minSrc);
	return Lerp(minDest, maxDest, t);
}