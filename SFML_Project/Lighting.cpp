#include "Lighting.h"

const Lighting::RGBLight Lighting::blackLight = { 0, 0, 0 };


Lighting::Lighting(Game& game) : game(game) {
	LIGHTS_WIDTH = game.WINDOW_WIDTH_TILES + 1;
	LIGHTS_HEIGHT = game.WINDOW_HEIGHT_TILES + 1;
	texture.create((unsigned int)(LIGHTS_WIDTH * game.TILE_WIDTH), (unsigned int)(LIGHTS_HEIGHT * game.TILE_HEIGHT));
	blurShader.loadFromFile("../SFML_Project/Shaders/gaussian_blur.frag", sf::Shader::Fragment);
	lightvalues.resize(LIGHTS_WIDTH * LIGHTS_HEIGHT, blackLight);
	oldLightvalues.resize(LIGHTS_WIDTH * LIGHTS_HEIGHT, blackLight);

	pixelData.resize(LIGHTS_WIDTH * game.TILE_WIDTH * LIGHTS_HEIGHT * game.TILE_HEIGHT * 4);
	sf::Uint32 black = 0xff000000;
	std::fill_n((sf::Uint32*)pixelData.data(), LIGHTS_WIDTH * game.TILE_WIDTH * LIGHTS_HEIGHT * game.TILE_HEIGHT, black);
}

void Lighting::ResetLights() {

	std::copy(lightvalues.begin(), lightvalues.end(), oldLightvalues.begin());
	std::fill(lightvalues.begin(), lightvalues.end(), blackLight);
}

void Lighting::SetLight(int x, int y, int val, int channel) {
	lightvalues[(x - game.tileTopLeft.x) + (y - game.tileTopLeft.y) * LIGHTS_WIDTH].Channel(channel) = val;
}

void Lighting::SpreadLight(int x, int y, int val, int channel) {
	if (val <= 0) return;
	RGBLight prev = lightvalues[(x - game.tileTopLeft.x) + (y - game.tileTopLeft.y) * LIGHTS_WIDTH];
	if (prev.Channel(channel) > val) return;
	// Let's think about this later
	if (game.map->At(x, y, 0).ambientLight && AMBIENT_LIGHT_VALUES[(int)game.timeOfDay].Channel(channel) > val) return;
	lightvalues[(x - game.tileTopLeft.x) + (y - game.tileTopLeft.y) * LIGHTS_WIDTH].Channel(channel) = val;
	if (x - 1 >= game.tileTopLeft.x) {
		SpreadLight(x - 1, y, (game.map->At(x - 1, y, 1).solid ? val - 3 : val - 1), channel);
	}
	if (y - 1 >= game.tileTopLeft.y) {
		SpreadLight(x, y - 1, (game.map->At(x, y - 1, 1).solid ? val - 3 : val - 1), channel);
	}
	if (x + 1 < game.tileTopLeft.x + LIGHTS_WIDTH && x + 1 < Level::MAX_WIDTH) {
		SpreadLight(x + 1, y, (game.map->At(x + 1, y, 1).solid ? val - 3 : val - 1), channel);
	}
	if (y + 1 < game.tileTopLeft.y + LIGHTS_HEIGHT && y + 1 < Level::MAX_HEIGHT) {
		SpreadLight(x, y + 1, (game.map->At(x, y + 1, 1).solid ? val - 3 : val - 1), channel);
	}
}

void Lighting::CalculateLights() {

	int currentHour = (int)game.timeOfDay;

	int valueR = AMBIENT_LIGHT_VALUES[currentHour].r;
	int valueG = AMBIENT_LIGHT_VALUES[currentHour].g;
	int valueB = AMBIENT_LIGHT_VALUES[currentHour].b;
	
	for (int x = game.tileTopLeft.x; x < game.tileTopLeft.x + LIGHTS_WIDTH; ++x) {
		for (int y = game.tileTopLeft.y; y < game.tileTopLeft.y + LIGHTS_HEIGHT; ++y) {

			if (game.map->IsValid(x, y, 1)) {
				if (game.map->At(x, y, 0).ambientLight) {
					SpreadLight(x, y, valueR, 0);
					SpreadLight(x, y, valueG, 1);
					SpreadLight(x, y, valueB, 2);
				}
				else if (game.map->At(x, y, 1).lightSource) {
					SpreadLight(x, y, game.map->At(x, y, 1).lightLevel, 0);
					SpreadLight(x, y, game.map->At(x, y, 1).lightLevel, 1);
					SpreadLight(x, y, game.map->At(x, y, 1).lightLevel, 2);
				}
			}
		}
	}

	if (game.player->lightLevel) {
		SpreadLight((int)(game.player->position.x / game.TILE_WIDTH), (int)(game.player->position.y / game.TILE_HEIGHT), game.player->lightLevel, 0);
		SpreadLight((int)(game.player->position.x / game.TILE_WIDTH), (int)(game.player->position.y / game.TILE_HEIGHT), game.player->lightLevel, 1);
		SpreadLight((int)(game.player->position.x / game.TILE_WIDTH), (int)(game.player->position.y / game.TILE_HEIGHT), game.player->lightLevel, 2);
	}
}

void Lighting::draw(sf::RenderTarget& target) {

	for (int x = 0; x < LIGHTS_WIDTH; ++x) {
		for (int y = 0; y < LIGHTS_HEIGHT; ++y) {
			// Let's assume max light is a divisor of 256
			int valueR = LIGHT_BRIGHTNESS[lightvalues[x + y * LIGHTS_WIDTH].r];
			int valueG = LIGHT_BRIGHTNESS[lightvalues[x + y * LIGHTS_WIDTH].g];
			int valueB = LIGHT_BRIGHTNESS[lightvalues[x + y * LIGHTS_WIDTH].b];
			// Don't update unchanged parts of the texture
			if (valueR != 0 && valueG != 0 && valueB != 0 &&
				valueR == LIGHT_BRIGHTNESS[oldLightvalues[x + y * LIGHTS_WIDTH].r] &&
				valueG == LIGHT_BRIGHTNESS[oldLightvalues[x + y * LIGHTS_WIDTH].g] &&
				valueB == LIGHT_BRIGHTNESS[oldLightvalues[x + y * LIGHTS_WIDTH].b]) {
				continue;
			}
			sf::Uint32 value32 = valueR | (valueG << 8) | (valueB << 16) | 0xff000000;
			// Do each row in one go
			for (int yy = 0; yy < game.TILE_HEIGHT; ++yy) {
				int index = (x * (int)game.TILE_WIDTH) * 4 + (y * (int)game.TILE_HEIGHT + yy) * LIGHTS_WIDTH * (int)game.TILE_WIDTH * 4;
				std::fill_n((sf::Uint32*) & pixelData[index], game.TILE_WIDTH, value32);
			}
		}
	}

	texture.update(pixelData.data());

	sf::Sprite sprite = sf::Sprite(texture);

	blurShader.setUniform("texture", sf::Shader::CurrentTexture);
	blurShader.setUniform("blur_radius", 0.01f);

	sf::RenderStates renderStates = sf::RenderStates(&blurShader);
	renderStates.blendMode = sf::BlendMultiply;
	renderStates.transform.translate(-game.renderOffset);

	target.draw(sprite, renderStates);
}


Lighting::~Lighting() {
}
