#include "Level.h"

#include <iostream>


const float Level::cutoffValues[Level::MAX_LEVELS] = {0.3f, -0.1f, -0.2f, -0.25f, -0.28f, -0.3f, -0.3f, -0.3f, -0.3f, -0.5f};
FastNoise Level::noiseGenerator = FastNoise(time(0));

Level::Level(int level) {
	if (level < 0) level = 0;
	if (level >= MAX_LEVELS) level = MAX_LEVELS - 1;
	this->level = level;
	lower = nullptr;
	upper = nullptr;
	roof = nullptr;
}

bool Level::IsValid(int x, int y, int z) const {
	return (x >= 0 && y >= 0 && x < MAX_WIDTH && y < MAX_HEIGHT && (z == 0 || z == 1));
}

void Level::Generate() {
	//if (tiles) delete[] tiles;
	lower = new Tile[MAX_WIDTH * MAX_HEIGHT];
	upper = new Tile[MAX_WIDTH * MAX_HEIGHT];
	roof = new bool[MAX_WIDTH * MAX_HEIGHT];
	for (int i = 0; i < MAX_WIDTH * MAX_HEIGHT; ++i) {
		if (level == 0) {
			Set(i % MAX_WIDTH, i / MAX_WIDTH, 0, Tile(TileType::Grass));
			At(i % MAX_WIDTH, i / MAX_WIDTH, 0).ambientLight = true;
		}
		else {
			Set(i % MAX_WIDTH, i / MAX_WIDTH, 0, Tile(TileType::Dirt));
		}
		Set(i % MAX_WIDTH, i / MAX_WIDTH, 1, Tile(TileType::Air));

		roof[i] = false;
	}

	float offset = level * 100.0f;

	for (int x = 0; x < MAX_WIDTH; ++x) {
		for (int y = 0; y < MAX_HEIGHT; ++y) {
			//Border
			if (x == 0 || y == 0 || x == MAX_WIDTH - 1 || y == MAX_HEIGHT - 1) {
				Set(x, y, 1, Tile(TileType::Stone));
			}
			// Spawn point for player
			else if (x == 1 && y == 1) {
				continue;
			}
			else {
				float value = noiseGenerator.GetNoise(x * 10.0f + level, y * 10.0f + level);
				if (value > cutoffValues[level]) {
					if (level == 0) {
						Set(x, y, 1, Tile(TileType::Dirt));
						At(x, y, 0).ambientLight = false;
					}
					else {
						Set(x, y, 1, Tile(TileType::Stone));
					}
				}
			}
		}
	}

}


Tile& Level::At(int x, int y, int z) const {
	if (IsValid(x, y, z)) {
		if (z == 0) {
			return lower[x + y * MAX_WIDTH];
		}
		else {
			return upper[x + y * MAX_WIDTH];
		}
	}
	else {
		std::cerr << "INVALID POSITION AT: " << x << " " << y << " " << z << std::endl;
	}
}

bool& Level::Roof(int x, int y) {
	if (IsValid(x, y, 0)) {
		return roof[x + y * MAX_WIDTH];
	}
	else {
		std::cerr << "INVALID POSITION AT: " << x << " " << y << std::endl;
	}
}

void Level::Set(int x, int y, int z, Tile newTile) {
	if (IsValid(x, y, z)) {
		if (z == 0) {
			lower[x + y * MAX_WIDTH] = newTile;
		}
		else {
			upper[x + y * MAX_WIDTH] = newTile;
		}
		SetNeighbourData(x, y, z);
		if (IsValid(x + 1, y, z)) {
			SetNeighbourData(x + 1, y, z);
		}
		if (IsValid(x, y - 1, z)) {
			SetNeighbourData(x, y - 1, z);
		}
		if (IsValid(x - 1, y, z)) {
			SetNeighbourData(x - 1, y, z);
		}
		if (IsValid(x, y + 1, z)) {
			SetNeighbourData(x, y + 1, z);
		}
	}
}

void Level::SetNeighbourData(int x, int y, int z) {
	if (At(x, y, z).type != TileType::Air) {

		At(x, y, z).neighbours = 0;

		if (IsValid(x + 1, y, z)) {
			if (At(x + 1, y, z).type == At(x, y, z).type) {
				At(x, y, z).neighbours |= 1 << 0;
			}
		}
		if (IsValid(x, y - 1, z)) {
			if (At(x, y - 1, z).type == At(x, y, z).type) {
				At(x, y, z).neighbours |= 1 << 1;
			}
		}
		if (IsValid(x - 1, y, z)) {
			if (At(x - 1, y, z).type == At(x, y, z).type) {
				At(x, y, z).neighbours |= 1 << 2;
			}
		}
		if (IsValid(x, y + 1, z)) {
			if (At(x, y + 1, z).type == At(x, y, z).type) {
				At(x, y, z).neighbours |= 1 << 3;
			}
		}

	}
}

Level::~Level() {
	if (lower) delete[] lower;
	if (upper) delete[] upper;
	if (roof) delete[] roof;
}
