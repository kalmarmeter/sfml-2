#include "Particle.h"



Particle::Particle(sf::Vector2f pos, sf::Vector2f vel, sf::Vector2f size, sf::Color col, float time) {
	position = pos;
	velocity = vel;
	color = col;
	this->size = size;
	timeToLive = time;
	inactive = false;
	vertices.setPrimitiveType(sf::TriangleStrip);
	for (int i = 0; i < 4; ++i) {
		//To keep clockwise order
		if (i > 1) {
			vertices.append(sf::Vertex(position + sf::Vector2f((1 - i % 2) * size.x, i / 2 * size.y), color));
		}
		else {
			vertices.append(sf::Vertex(position + sf::Vector2f(i % 2 * size.x, i / 2 * size.y), color));
		}
	}
}

void Particle::Update() {
	if (!inactive) {

		position += velocity;
		velocity *= 0.9f; //89% at 60fps

		timeToLive -= 0.0167f;
		if (timeToLive < 0.0f) inactive = true;
	}
}

void Particle::UpdateVertices() {
	for (int i = 0; i < 4; ++i) {
		vertices[i].position = sf::Vector2f(position + sf::Vector2f(i % 2 * size.x, i / 2 * size.y));
	}
}

void Particle::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(vertices, states);
}

Particle::~Particle()
{
}
