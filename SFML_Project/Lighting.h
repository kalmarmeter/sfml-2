#pragma once

#include <SFML/Graphics.hpp>

#include <vector>

class Game;

class Lighting {
public:
	struct RGBLight {
		int r, g, b;
		int& Channel(int ch) {
			if (ch == 0) return r;
			if (ch == 1) return g;
			return b;
		}
		const int& Channel(int ch) const {
			if (ch == 0) return r;
			if (ch == 1) return g;
			return b;
		}
	};

	static const RGBLight blackLight;

private:

	std::vector<RGBLight> lightvalues;
	std::vector<RGBLight> oldLightvalues;
	std::vector<sf::Uint8> pixelData;
	sf::Texture texture;
	sf::Shader blurShader;
	Game& game;
	bool firstDraw = true;

	void SpreadLight(int x, int y, int val, int channel);
	void SetLight(int x, int y, int val, int channel);

public:

	static const int MAX_LIGHT_VALUE = 16;
	//const int LIGHT_BRIGHTNESS[MAX_LIGHT_VALUE] = {0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 215, 225, 235, 245, 255};
	const int LIGHT_BRIGHTNESS[MAX_LIGHT_VALUE] = {0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 195, 210, 230, 255};
	const RGBLight AMBIENT_LIGHT_VALUES[24] = {
		{0, 0, 1},			//0
		{0, 0, 0},			//1
		{0, 0, 0},			//2
		{0, 0, 2},			//3
		{1, 1, 3},			//4
		{4, 4, 6},			//5
		{7, 7, 9},			//6
		{11, 13, 13},		//7
		{12, 13, 12},		//8
		{12, 13, 10},		//9
		{14, 13, 9},		//10
		{14, 14, 9},		//11
		{15, 14, 7},		//12
		{15, 14, 7},		//13
		{15, 13, 6},		//14
		{15, 12, 6},		//15
		{14, 11, 6},		//16
		{14, 11, 6},		//17
		{14, 10, 6},		//18
		{14, 9, 5},			//19
		{11, 5, 4},			//20
		{8, 4, 5},			//21
		{2, 2, 5},			//22
		{1, 1, 3}			//23
	};
	int LIGHTS_WIDTH;
	int LIGHTS_HEIGHT;


	Lighting(Game& game);
	void ResetLights();
	void CalculateLights();
	void draw(sf::RenderTarget& target);
	~Lighting();
};

#include "Game.h"
