#include "Game.h"

void Game::DebugMessage(const std::string& msg) const {
	std::cout << "[DEBUG]" << msg << std::endl;
}

Game::Game() : particleManager(*this), entityManager(*this) {

	Initialize();

}

void Game::Initialize() {

	window = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Title");
	renderTexture.create(WINDOW_WIDTH, WINDOW_HEIGHT);
	//window->setFramerateLimit(60);
	window->setKeyRepeatEnabled(false);
	clock = new sf::Clock();
	lastTime = clock->restart();
	font.loadFromFile("../SFML_Project/Resources/Pixeled.ttf");
	tilesFloorTexture.loadFromFile("../SFML_Project/Resources/tiles_floor.png");
	tilesWallTexture.loadFromFile("../SFML_Project/Resources/tiles_wall.png");
	playerTexture.loadFromFile("../SFML_Project/Resources/player.png");
	ditherShader.loadFromFile("../SFML_Project/Shaders/dither.frag", sf::Shader::Fragment);
	keyboardState = new KeyState[sf::Keyboard::KeyCount];
	for (int i = 0; i < sf::Keyboard::KeyCount; ++i) {
		keyboardState[i] = KeyState::NotPressed;
	}

	map = new Map();
	player = new Player(sf::Vector2f(48.0f, 48.0f), 0.0f, sf::Vector2f(24.0f, 24.0f), *this);
	lighting = new Lighting(*this);
	EntityAtlas::Load();

	map->Set((int)player->position.x / TILE_WIDTH, (int)player->position.y / TILE_HEIGHT, 1, Tile(TileType::Air));

	playing = true;

	while (playing) {
		frameTime = lastTime.asSeconds();
		secondsPassed += frameTime;
		lag += frameTime;
		MainLoop();
		lastTime = clock->restart();
	}

	window->close();

}

void Game::GetKeyboardState() {
	for (int i = 0; i < sf::Keyboard::KeyCount; ++i) {
		if (sf::Keyboard::isKeyPressed((sf::Keyboard::Key)i)) {
			if (keyboardState[i] == KeyState::NotPressed) {
				keyboardState[i] = KeyState::Pressed;
			}
			else if (keyboardState[i] == KeyState::Pressed) {
				keyboardState[i] = KeyState::Held;
			}
		}
		else {
			keyboardState[i] = KeyState::NotPressed;
		}
	}
}

void Game::ProcessInput() {
	if (keyboardState[sf::Keyboard::W] == KeyState::Held) {
		player->AddVelocity(sf::Vector2f(0.0f, -player->movementSpeed));
	}
	if (keyboardState[sf::Keyboard::A] == KeyState::Held) {
		player->AddVelocity(sf::Vector2f(-player->movementSpeed, 0.0f));
	}
	if (keyboardState[sf::Keyboard::S] == KeyState::Held) {
		player->AddVelocity(sf::Vector2f(0.0f, +player->movementSpeed));
	}
	if (keyboardState[sf::Keyboard::D] == KeyState::Held) {
		player->AddVelocity(sf::Vector2f(+player->movementSpeed, 0.0f));
	}
	if (keyboardState[sf::Keyboard::Space] == KeyState::Pressed) {
		entityManager.Clear();
		particleManager.Clear();
	}
	if (keyboardState[sf::Keyboard::E] == KeyState::Pressed) {
		raining = !raining;
	}
	if (keyboardState[sf::Keyboard::R] == KeyState::Pressed) {
		map->Roof((int)(mousePosition.x / TILE_WIDTH), (int)(mousePosition.y / TILE_HEIGHT)) = !map->Roof((int)(mousePosition.x / TILE_WIDTH), (int)(mousePosition.y / TILE_HEIGHT));
	}
	if (keyboardState[sf::Keyboard::L] == KeyState::Pressed) {
		player->lightLevel = (player->lightLevel) ? 0 : 8;
	}
	if (keyboardState[sf::Keyboard::T] == KeyState::Pressed) {
		entityManager.NewEntity(EntityType::Slime, sf::Vector2f(mousePosition), 0.0f);
	}
}

void Game::DrawFPS() {
	sf::Text fpsText = sf::Text(std::to_string(1.0f / frameTime), font);
	fpsText.setCharacterSize(12);
	fpsText.setPosition(0.0f, 5.0f);
	window->draw(fpsText);
}

void Game::DrawMouseCoords() {

}

void Game::HandleMousePress(const sf::Event::MouseButtonEvent& event) {
	switch (event.button) {
	case sf::Mouse::Left: {
		int x = (int)(mousePosition.x / TILE_WIDTH);
		int y = (int)(mousePosition.y / TILE_HEIGHT);
		map->Set(x, y, 1, (map->At(x, y, 1).type == TileType::Air) ? Tile(TileType::Stone) : Tile(TileType::Air));
		//map->Set(x, y, 0, (map->At(x, y, 0).type == TileType::Dirt) ? Tile(TileType::Stone) : Tile(TileType::Dirt));
		break;
	}
	case sf::Mouse::Right: {
		chargeBeginPosition = mousePosition;
		charging = true;
		break;
		}
	}
	
}

void Game::HandleMouseRelease(const sf::Event::MouseButtonEvent& event) {
	switch (event.button) {
	case sf::Mouse::Left:
		break;
	case sf::Mouse::Right:
		if (charging) {
			Entity* entity;
			if (entity = entityManager.NewEntity(EntityType::Arrow, player->position, Util::PI + Util::VectorToDirection(sf::Vector2f(mousePosition.x - chargeBeginPosition.x, chargeBeginPosition.y - mousePosition.y)))) {
				sf::Vector2f direction = Util::Normalize(Util::DirectionToVector(-entity->direction));
				float velocity = Util::Magnitude(sf::Vector2f(mousePosition.x - chargeBeginPosition.x, chargeBeginPosition.y - mousePosition.y)) * 0.01f;
				Util::Clamp(arrowSpeedMin, arrowSpeedMax, velocity);
				entity->SetVelocity(direction * velocity);
			}
			charging = false;
		}
		break;
	}
}

void Game::HandleMouseMove(const sf::Event& event) {
	mousePosition = sf::Mouse::getPosition(*window) + sf::Vector2i(renderTopLeft.x, renderTopLeft.y);
}

void Game::MainLoop() {

	sf::Clock debugClock;

	window->clear();
	renderTexture.clear();

	player->SetVelocity(sf::Vector2f());

	sf::Event event;
	while (window->pollEvent(event)) {

		switch (event.type) {
		case sf::Event::Closed:
			playing = false;
			return;
		case sf::Event::MouseMoved:
			HandleMouseMove(event);
			break;
		case sf::Event::MouseButtonPressed:
			HandleMousePress(event.mouseButton);
			break;
		case sf::Event::MouseButtonReleased:
			HandleMouseRelease(event.mouseButton);
			break;
		}

	}

	GetKeyboardState();

	ProcessInput();

	DebugMessage("Event and input handling: " + std::to_string(debugClock.restart().asMilliseconds()));

	while (lag >= TARGET_FRAME_TIME) {

		timeOfDay += TARGET_FRAME_TIME;
		if (timeOfDay > 24.0f) timeOfDay -= 24.0f;

		player->Update();

		renderTopLeft = player->position + sf::Vector2f(-WINDOW_WIDTH / 2.0f, -WINDOW_HEIGHT / 2.0f);
		if (renderTopLeft.x < 0.0f) renderTopLeft.x = 0.0f;
		if (renderTopLeft.y < 0.0f) renderTopLeft.y = 0.0f;
		if (renderTopLeft.x + WINDOW_WIDTH > Level::MAX_WIDTH * TILE_WIDTH) renderTopLeft.x = Level::MAX_WIDTH * TILE_WIDTH - WINDOW_WIDTH;
		if (renderTopLeft.y + WINDOW_HEIGHT > Level::MAX_HEIGHT * TILE_HEIGHT) renderTopLeft.y = Level::MAX_HEIGHT * TILE_HEIGHT - WINDOW_HEIGHT;

		tileTopLeft = sf::Vector2i((int)(renderTopLeft.x / TILE_WIDTH), (int)(renderTopLeft.y / TILE_HEIGHT));

		renderOffset = sf::Vector2f(renderTopLeft.x - tileTopLeft.x * TILE_WIDTH, renderTopLeft.y - tileTopLeft.y * TILE_HEIGHT);

		DebugMessage("Player update: " + std::to_string(debugClock.restart().asMilliseconds()));

		entityManager.Update();

		DebugMessage("Entities update: " + std::to_string(debugClock.restart().asMilliseconds()));

		particleManager.Update();

		DebugMessage("Particle update: " + std::to_string(debugClock.restart().asMilliseconds()));

		lag -= TARGET_FRAME_TIME;
	}

	lighting->ResetLights();
	lighting->CalculateLights();

	DebugMessage("Lighting update: " + std::to_string(debugClock.restart().asMilliseconds()));

	underRoof = map->Roof((int)(player->position.x / TILE_WIDTH), (int)(player->position.y / TILE_HEIGHT));

	sf::Sprite sprFloor = sf::Sprite(tilesFloorTexture);
	sf::Sprite sprWall = sf::Sprite(tilesWallTexture);

	for (int x = tileTopLeft.x; x < tileTopLeft.x + WINDOW_WIDTH_TILES + 1; ++x) {

		for (int y = tileTopLeft.y; y < tileTopLeft.y + WINDOW_HEIGHT_TILES + 1; ++y) {

			if (map->IsValid(x, y, 0)) {

				if (underRoof != map->Roof(x, y)) {
					sprFloor.setColor(sf::Color(100, 100, 100));
					sprWall.setColor(sf::Color(100, 100, 100));
				}
				else {
					sprFloor.setColor(sf::Color::White);
					sprWall.setColor(sf::Color::White);
				}

				TileType tileType = map->At(x, y, 0).type;
				sprFloor.setPosition(x * TILE_WIDTH - renderTopLeft.x, y * TILE_HEIGHT - renderTopLeft.y);
				sprFloor.setTextureRect(sf::IntRect((int)tileType * TILE_WIDTH, map->At(x, y, 0).neighbours * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT));

				renderTexture.draw(sprFloor);

				tileType = map->At(x, y, 1).type;
				if (tileType != TileType::Air) {
					sprWall.setPosition(x * TILE_WIDTH - renderTopLeft.x, y * TILE_HEIGHT - renderTopLeft.y);
					sprWall.setTextureRect(sf::IntRect((int)tileType * TILE_WIDTH, map->At(x, y, 1).neighbours * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT));
					renderTexture.draw(sprWall);
				}
				
				if (raining && map->GetCurrentLevel() == 0 && !map->Roof(x, y) && Util::RandomInt(0, 512) == 0) {
					particleManager.NewParticles(3, Util::RandomVec(sf::Vector2f(x * TILE_WIDTH, y * TILE_HEIGHT), TILE_WIDTH / 2.0f, TILE_HEIGHT / 2.0f), 0.0f, Util::TWO_PI, 2.0f, sf::Vector2f(3.0f, 3.0f), sf::Color::Blue, 1.0f);
				}
			}
		}

	}

	DebugMessage("Tiles drawing: " + std::to_string(debugClock.restart().asMilliseconds()));

	
	sf::Sprite sprPlayer = sf::Sprite(playerTexture);

	sprPlayer.setOrigin(8.0f, 16.0f);
	sprPlayer.setRotation(Util::VectorToDirection(sf::Vector2f(mousePosition) - player->position) * Util::RAD);
	sprPlayer.setPosition(player->position - renderTopLeft);

	renderTexture.draw(sprPlayer);

	renderTexture.draw(entityManager);

	DebugMessage("Entities drawing: " + std::to_string(debugClock.restart().asMilliseconds()));

	renderTexture.draw(particleManager);

	DebugMessage("Particles drawing: " + std::to_string(debugClock.restart().asMilliseconds()));

	lighting->draw(renderTexture);

	DebugMessage("Lighting drawing: " + std::to_string(debugClock.restart().asMilliseconds()));

	DrawFPS();

	sf::Text moneyText(std::to_string(player->money), font);
	moneyText.setCharacterSize(12);
	moneyText.setPosition(0.0f, 24.0f);

	renderTexture.draw(moneyText);

	//TEMP
	moneyText = sf::Text(std::to_string((int)timeOfDay), font);
	moneyText.setPosition(0.0f, 48.0f);

	renderTexture.draw(moneyText);

	if (charging) {
		sf::RectangleShape rectangleShape;
		rectangleShape.setOrigin(rectangleShape.getLocalBounds().width / 2.0f, rectangleShape.getLocalBounds().height / 2.0f);
		rectangleShape.setSize(sf::Vector2f(Util::Magnitude(sf::Vector2f(mousePosition.x - chargeBeginPosition.x, chargeBeginPosition.y - mousePosition.y)) * 0.5f, 2.0f));
		rectangleShape.setRotation(Util::VectorToDirection(sf::Vector2f(mousePosition - chargeBeginPosition)) * Util::RAD);
		rectangleShape.setPosition(sf::Vector2f(mousePosition + chargeBeginPosition) / 2.0f - renderTopLeft);
		rectangleShape.setFillColor(sf::Color::Red);

		renderTexture.draw(rectangleShape);
	}

	renderTexture.display();
	//window->draw(sf::Sprite(renderTexture.getTexture()), &ditherShader);
	window->draw(sf::Sprite(renderTexture.getTexture()));

	window->display();

}

Game::~Game() {

	if (map) delete map;
	if (window) delete window;
	if (clock) delete clock;
	if (player) delete player;
	if (keyboardState) delete[] keyboardState;
	if (lighting) delete lighting;

}
