#pragma once

#include "Tile.h"
#include "FastNoise.h"


class Level {
public:

	static const int MAX_WIDTH = 64;
	static const int MAX_HEIGHT = 64;
	static const int MAX_LEVELS = 10;

	Level(int level = 0);
	void Generate();
	Tile& At(int x, int y, int z) const;
	void Set(int x, int y, int z, Tile newTile);
	bool IsValid(int x, int y, int z) const;
	bool& Roof(int x, int y);
	~Level();

private:

	static const float cutoffValues[MAX_LEVELS];
	static FastNoise noiseGenerator;

	int level;

	Tile* lower;
	Tile* upper;
	bool* roof;

	void SetNeighbourData(int x, int y, int z);


};

