#pragma once

enum class KeyState {
	NotPressed, Pressed, Held
};