#pragma once

#include <SFML/Graphics.hpp>

#include <vector>

#include "Particle.h"
#include "Util.h"

class Game;

class ParticleManager : public sf::Drawable {
private:
	std::vector<Particle> particles;
	static const int MAX_PARTICLES = 1'000;
	Game& game;
public:
	ParticleManager(Game& game);
	void NewParticle(sf::Vector2f pos, sf::Vector2f vel, sf::Vector2f size, sf::Color col, float time = 3.0f);
	void NewParticles(int num, sf::Vector2f pos, float dir, float angle, float vel, sf::Vector2f size, sf::Color col, float time = 3.0f);
	void Clear();
	void Update();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	~ParticleManager();
};


#include "Game.h"
