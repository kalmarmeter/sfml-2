#pragma once

enum class EntityType {

	Player = -1,
	Arrow,
	Slime,
	Coin

};