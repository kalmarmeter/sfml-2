#pragma once

#include "Level.h"

class Map {
private:

	Level* levels;
	int currentLevel;

public:
	Map();
	const Tile& At(int x, int y, int z) const;
	void Set(int x, int y, int z, Tile newTile);
	bool& Roof(int x, int y);
	bool IsValid(int x, int y, int z) const;
	int GetCurrentLevel() const;
	~Map();
};

