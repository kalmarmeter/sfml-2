#pragma once

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <vector>

#include "Entity.h"


struct EntityProperties {
	int type;
	sf::Vector2f size;
	bool directionDependent;
	sf::IntRect textureRect;
	int timeToLive = -1;
};


class EntityAtlas {
private:
	static std::vector<EntityProperties> entityProperties;
public:
	static void Load();
	static void SetValues(Entity& entity);
	static sf::IntRect GetTextureRect(Entity& entity);
};

