#pragma once

enum class TileType {
	Air = 0,
	Dirt,
	Stone,
	Grass,
	//Torch
};