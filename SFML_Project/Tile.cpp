#include "Tile.h"

Tile::Tile(TileType type) {
	this->type = type;
	switch (type) {
	case TileType::Air:
		solid = false;
		color = sf::Color();
		lightSource = false;
		lightLevel = 0;
		break;
	case TileType::Dirt:
		solid = true;
		color = sf::Color(164, 90, 44);
		lightSource = false;
		lightLevel = 0;
		break;
	case TileType::Stone:
		solid = true;
		color = sf::Color(64, 64, 64);
		lightSource = false;
		lightLevel = 0;
		break;
	case TileType::Grass:
		solid = true;
		color = sf::Color(124, 204, 55);
		lightSource = false;
		lightLevel = 0;
		break;
	}
}