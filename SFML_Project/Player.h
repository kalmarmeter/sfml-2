#pragma once

#include "Entity.h"

class Player : public Entity {
public:
	Player(sf::Vector2f pos, float dir, sf::Vector2f size, Game& game);
	float movementSpeed = 2.0f;
	int lightLevel = 3;
	int money = 0;
};

