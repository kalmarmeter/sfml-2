#pragma once

#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector2.hpp>

#include "EntityType.h"

class Game;

class Entity {
protected:
	Game& game;

	void UpdateAABB();
	void CheckTileCollisions();
	void CheckEntityCollisions();
	void CheckPlayerCollision();

public:


	EntityType type;

	sf::Vector2f position;
	float direction;
	sf::Vector2f size;
	sf::Vector2f velocity;
	sf::FloatRect AABB;
	sf::Vector2f AABBOffset;

	sf::Vector2i tileCollision;
	Entity* entityCollision;
	bool playerCollision;

	int timeToLive = -1;
	bool inactive = false;
	bool directionDependent;

	Entity(EntityType type, sf::Vector2f pos, float dir, sf::Vector2f size, Game& game, bool directionDependentCollision = false);
	Entity& operator=(const Entity& other);
	void Update();
	void AddVelocity(const sf::Vector2f& velocityToAdd);
	void SetVelocity(const sf::Vector2f& newVelocity);

	~Entity();

};

#include "Game.h"
