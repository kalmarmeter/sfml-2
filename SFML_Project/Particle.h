#pragma once

#include <SFML/Graphics.hpp>

class Particle : public sf::Drawable {
public:

	sf::Vector2f position;
	sf::Vector2f velocity;
	sf::Vector2f size;
	sf::Color color;
	sf::VertexArray vertices;
	float timeToLive;
	bool inactive;

	Particle(sf::Vector2f pos, sf::Vector2f vel, sf::Vector2f size, sf::Color col, float time = 3.0f);
	void Update();
	void UpdateVertices();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	~Particle();
};

