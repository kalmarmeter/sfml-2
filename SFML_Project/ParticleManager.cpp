#include "ParticleManager.h"



ParticleManager::ParticleManager(Game& game) : game(game) {
	particles.reserve(MAX_PARTICLES);
}

void ParticleManager::NewParticle(sf::Vector2f pos, sf::Vector2f vel, sf::Vector2f size, sf::Color col, float time) {
	for (int i = 0; i < MAX_PARTICLES; ++i) {
		if (i < particles.size() && particles[i].inactive) {
			particles[i] = Particle(pos, vel, size, col, time);
			break;
		}
		else if (i == particles.size() && i < MAX_PARTICLES) {
			particles.push_back(Particle(pos, vel, size, col, time));
			break;
		}
	}
}

void ParticleManager::NewParticles(int num, sf::Vector2f pos, float dir, float angle, float vel, sf::Vector2f size, sf::Color col, float time) {
	for (int i = 0; i < num; ++i) {
		float randomAngle = Util::RandomFloat(-angle / 2.0f, angle / 2.0f);
		float randomDir = dir + randomAngle;
		sf::Vector2f velocity = Util::DirectionToVector(randomDir) * vel;
		NewParticle(pos, velocity, size, col, time);
	}
}

void ParticleManager::Update() {
	for (Particle& particle : particles) {
		if (!particle.inactive) {
			particle.Update();
		}
	}
}

void ParticleManager::Clear() {
	particles.clear();
}

void ParticleManager::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	for (Particle particle : particles) {
		if (!particle.inactive) {
			particle.position -= game.renderTopLeft;
			particle.UpdateVertices();
			target.draw(particle);
		}
	}
}

ParticleManager::~ParticleManager()
{
}
